import java.util.Map;

public class test {
    //考虑无效输入，比如输入的数组参数为空指针、数组长度<=0等，此时如果让函数返回0，但是又怎么区分最大值为0的情况呢，
    //所以，定义一个全局变量来标记是否输入无误
    boolean g_InvalidInput=false;
    public int FindGreatestSumOfSubArray11(int[] array) {
        if (array.length<=0) {
            g_InvalidInput=true;
            return 0;
        }
        int nCurSum=0;//标记过程中的累加值
        int nGreatestSum=0x80000000;//表示int型中最小的值，值为-2^31
        for(int i=0;i<array.length;i++) {
            if (nCurSum<=0) {//表示前面的累加为负数，可以舍弃
                nCurSum=array[i];
            }else {
                nCurSum+=array[i];
            }
            if (nCurSum>nGreatestSum) {//与现有最大值比较
                nGreatestSum=nCurSum;
            }
        }
        return nGreatestSum;
    }
    /**
     * 遍历数据
     * @param array 输入数组
     * @return 输出连续和的最大值
     */
    public static int FindGreatestSumOfSubArray1(int[] array){
        int max = -99999;

        if(array.length < 1){
            return max;
        }
        for(int i=0; i<array.length; i++){
            int temp1 = 0;
            for(int j=i; j<array.length; j++){
                temp1 += array[j];
                if(temp1 > max){
                    max = temp1;
                }
            }
        }
        return max;
    }

    /**
     * 动态规划
     * @param array
     * @return
     */
    public int FindGreatestSumOfSubArray(int[] array){
        int max = array[0];
        int res = array[0];
        for(int i=1; i<array.length; i++){
            max = (max+array[i]) > array[i] ? max+array[i] : array[i];
            res = max>res ? max : res;
        }
        return res;
    }


    public static void main(String[] args) {
        int arr[]= {1,-2,3,10,-4,7,2,-5};
        test fArray=new test();
        int res=fArray.FindGreatestSumOfSubArray11(arr);
        System.out.println(res);
       // int max ＝Integer.MAX_VALUE;//最大值
       // int min ＝Integer.MIN_VALUE;//最小值
        int i = FindGreatestSumOfSubArray1(arr);
        System.out.println(i);
    }
}
