package com.demo.test01;


/**
 * @author Administrator
 *单例模式测试,结果都为true
 */
public class Test02 {
	
	public static void main(String[] args) {
		TestSingleton instance2 = TestSingleton.getInstance();
		TestSingleton instance3 = TestSingleton.getInstance();
		System.out.println(instance2.equals(instance3));
		System.out.println(instance2==instance3);
	}
}
