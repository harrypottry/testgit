package com.demo.test01;

/**
 * @author Administrator
 *单例模式，多例模式
	所谓单例就是所有的请求都用一个对象来处理，比如我们常用的service和dao层的对象通常都是单例的，
	而多例则指每个请求用一个新的对象来处理，比如action; 
 */
public class Singleton {
	
	/**
	 * 懒汉式单例
	 * 
	 */
	private Singleton(){}
	
	private static Singleton singleton = null;
	
	public static Singleton getInstance() {
		if (singleton==null) {
			singleton = new Singleton();
		}
		return singleton;
	}
	
	
}
