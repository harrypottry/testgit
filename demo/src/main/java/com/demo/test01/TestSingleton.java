package com.demo.test01;

/**
 * @author Administrator
 *饿汉式单例模式
 */
public class TestSingleton {
	

	//私有构造，防止外部new
	private TestSingleton(){

	}
	
	//在类创建的同时，就已经创建好一个静态对象供系统使用，以后不再改变
	//所以天生是线程安全的
	
	private static final TestSingleton single = new TestSingleton();
	
	public static TestSingleton getInstance(){
		return single;
	}
	
}
