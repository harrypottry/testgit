package com.example.bio;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerRefactor {
    public static void main(String[] args) {
        RequestHandler requestHandler = new RequestHandler();
        try (ServerSocket serverSocket = new ServerSocket(8888)){
            System.out.println("bioServer has started,listening on port"+serverSocket.getLocalSocketAddress());
            while (true){
                Socket clientSocket = serverSocket.accept();
                //专门处理客户端的连接请求
                new ClientHandler(clientSocket,requestHandler).run();
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
