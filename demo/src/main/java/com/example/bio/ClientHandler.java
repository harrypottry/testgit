package com.example.bio;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class ClientHandler implements Runnable {
    private final Socket clientSocket;
    private final RequestHandler requestHandler;

    @Override
    public void run() {
        System.out.println("Connection from "+ clientSocket.getRemoteSocketAddress());
        try(Scanner input = new Scanner(clientSocket.getInputStream())){
            //Server Client loop 交互
            while (true){
                String request = input.nextLine();
                if ("quit".equals(request)){
                    break;
                }
                System.out.println(String.format("From %s :%s",clientSocket.getRemoteSocketAddress(),request));
               // String response = "From bioServer Hello" + request + ".\n";
                String response = requestHandler.handle(request);
                clientSocket.getOutputStream().write(response.getBytes());
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public ClientHandler(Socket clientSocket, RequestHandler requestHandler) {
        this.clientSocket = clientSocket;
        this.requestHandler = requestHandler;
    }
}
