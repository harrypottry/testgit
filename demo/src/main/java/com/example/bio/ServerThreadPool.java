package com.example.bio;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerThreadPool {
    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(3);
        RequestHandler requestHandler = new RequestHandler();
        try(ServerSocket serverSocket = new ServerSocket(8888)){
            System.out.println("bioServer has started,listening on port:" + serverSocket.getLocalSocketAddress());
            while (true){
                //原始方法，只要客户端与服务器建立连接就多线程数据交互,问题是，客户端不是一直不停的在数据交互，所以没有必要一直开启线程，造成资源浪费

                Socket clientSocket = serverSocket.accept();
                //2.0 版本 ，稍等，先别开线程,
                //所以引入nio ，java1.4之后封装的方法，去判断socket的的状态，通过channel放入map中，时候需要数据交互，然后执行

                executor.submit(new ClientHandler(clientSocket,requestHandler));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
