package com.example.demo;

import java.util.Arrays;

public class MaxAddOfArray {

    public static void main(String[] args) {
        int[] arr = new int[]{1,-2,3,10,-4,7,2,-5};
        int[] ret = maxAddOfArray(arr);
        System.out.println("maxSum: " + ret[0]);
        System.out.println("start: " + ret[1]); //开始下标
        System.out.println("end: " + ret[2]); //结束下标

        int [] newData;
        newData = Arrays.copyOfRange(arr,ret[1]-1,ret[2]);   //array.copyofrange 截取范围:[start,end)
        for (int i:newData){
            System.out.println(i);
        }
    }

    public static int[] maxAddOfArray(int[] arr) {
        int[] ret = new int[3];
        if (arr == null || arr.length == 0) {
            return new int[]{0, 0, 0};
        }

        int max = arr[0];
        int sum = arr[0];
        int start = 0;
        int end = 0;
        for (int i = 1; i < arr.length; i++) {
            if (sum < 0) {
                sum = arr[i];
                start = i;
            } else {
                sum += arr[i];
            }
            if (sum > max) {
                end = i;
                max = sum;
            }
        }
        ret[0] = max;
        ret[1] = start;
        ret[2] = end;
        return ret;
    }
}
