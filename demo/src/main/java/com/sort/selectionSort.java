package com.sort;

/**
 * 最佳情况：T(n) = O(n2)  最差情况：T(n) = O(n2)  平均情况：T(n) = O(n2)
 */
public class selectionSort {
    public static int[] selectionSort(int[] array){
        if (array.length == 0){
            return array;
        }
        for (int i = 0;i<array.length;i++){
            int minIndex = i;
            for (int j = i;j<array.length;j++){
                if (array[j]<array[minIndex]){
                    minIndex =j;
                }
            }
            if (minIndex != i){
                int temp = array[minIndex];
                array[minIndex] = array[i];
                array[i] =temp;
            }
        }
        return array;
    }
}
