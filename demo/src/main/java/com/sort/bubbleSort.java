package com.sort;

/**
 *最佳情况：T(n) = O(n)? ?最差情况：T(n) = O(n2)? ?平均情况：T(n) = O(n2)
 */
public class bubbleSort {
    public static int[] bubbleSort(int[] array){
        if (array.length == 0){
            return array;
        }
        for (int i = 0;i<array.length;i++){
            for (int j =0;j<array.length-1-i;j++){
                if (array[j+1] < array[j]){
                    int temp = array[j+1];
                    array[j+1] = array[j];
                    array[j] = temp;
                }
            }
        }
        return array;
    }
}
