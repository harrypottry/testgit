package com.sort;


public class quickSort {
    /**
     * 快速排序方法
     * @param array
     * @param start
     * @param end
     * @return
     */
    public static int[] quickSort(int[] array,int start,int end){
        if (array.length<1 || start< 0  || end>= array.length || start>end){
            return null;
        }
        int smailIndex = partition(array,start,end);
        if (smailIndex >start){
            quickSort(array,start,smailIndex-1);
        }
        if (smailIndex<end){
            quickSort(array,smailIndex+1,end);
        }
        return array;
    }
    /**
     * 快速排序算法
     *Java中Math类的random()方法可以生成[0,1)之间的随机浮点数
     * (int)Math.random() * (end-start+1)生成从start到end的随机整数[start,end]
     */
    public static  int partition(int[] array ,int start ,int end){
        int pivot = (int)(start + Math.random() * (end-start+1));
        int smailIndex = start -1;
        swap(array,pivot,end);//org.apache.commons.lang3.ArrayUtils.swap
        for (int i = start;i<=end;i++){
            if (array[i] <= array[end]){
                smailIndex++;
                if (i > smailIndex){
                    swap(array,i,smailIndex);
                }
            }
        }
        return smailIndex;
    }
    /**
     * 交换数组内的两个元素
     * 这里没有采用封装的方法
     */
    public  static  void swap(int[] array, int i ,int j){
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
