package com.tree;

import java.util.ArrayList;

public class digui {
    public ArrayList qianxuNumList = new ArrayList();
    public ArrayList zhongxuNumList = new ArrayList();
    public ArrayList houxuNumList = new ArrayList();

    // 用递归的方法进行先序遍历
    public void qinaxuDigui(TreeNode treeNode) {
        qianxuNumList.add(treeNode.val);
        if (treeNode.left != null) {
            qinaxuDigui(treeNode.left);
        }
        if (treeNode.right != null) {
            qinaxuDigui(treeNode.right);
        }
    }
    // 用递归的方法进行中序遍历
    public void zhongxuDigui(TreeNode treeNode) {
        if (treeNode.left != null) {
            zhongxuDigui(treeNode.left);
        }
        zhongxuNumList.add(treeNode.val);
        if (treeNode.right != null) {
            zhongxuDigui(treeNode.right);
        }
    }

    // 用递归的方法进行后序遍历
    public void houxuDigui(TreeNode treeNode) {
        if (treeNode.left != null) {
            houxuDigui(treeNode.left);
        }
        if (treeNode.right != null) {
            houxuDigui(treeNode.right);
        }
        houxuNumList.add(treeNode.val);
    }
}
