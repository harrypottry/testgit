package com.tree;

import java.util.ArrayList;
import java.util.Stack;

public class stack {
    //push进栈
    //pop出栈
    //pick取出当前栈最上面的数据
    public ArrayList qianxuNumList = new ArrayList();
    public ArrayList zhongxuNumList = new ArrayList();
    public ArrayList houxuNumList = new ArrayList();

    // 用非递归的方法进行先序遍历
    public void qinaxuFeiDigui(TreeNode treeNode) {
        Stack<TreeNode> stack = new Stack<>();
        while (treeNode != null || !stack.isEmpty()) {
            while (treeNode != null) {
                qianxuNumList.add(treeNode.val);
                stack.push(treeNode);
                treeNode = treeNode.left;
            }
            if(!stack.isEmpty()){
                treeNode = stack.pop();
                treeNode = treeNode.right;
            }
        }
    }

    // 用非递归的方法进行中序遍历
    public void zhongxuFeiDigui(TreeNode treeNode) {
        Stack<TreeNode> stack = new Stack<>();
        while (treeNode != null || !stack.isEmpty()) {
            while (treeNode != null) {
                stack.push(treeNode);
                treeNode = treeNode.left;
            }
            if (!stack.isEmpty()) {
                treeNode = stack.pop();
                zhongxuNumList.add(treeNode.val);
                treeNode = treeNode.right;
            }
        }
    }

    // 用非递归的方法进行后序遍历
    public void houxuFeiDigui(TreeNode treeNode) {
        Stack<TreeNode> stack = new Stack<>();
        while (treeNode != null || !stack.isEmpty()) {
            while (treeNode != null) {
                stack.push(treeNode);
                treeNode = treeNode.left;
            }
            boolean tag = true;
            TreeNode preNode = null;  // 前驱节点
            while (!stack.isEmpty() && tag == true) {
                treeNode = stack.peek();
                if (treeNode.right == preNode) {  // 之前访问的为空节点或是栈顶节点的右子节点
                    treeNode = stack.pop();
                    houxuNumList.add(treeNode.val);
                    if (stack.isEmpty()) {
                        return;
                    } else {
                        preNode = treeNode;
                    }
                } else {
                    treeNode = treeNode.right;
                    tag = false;
                }
            }
        }
    }

}
